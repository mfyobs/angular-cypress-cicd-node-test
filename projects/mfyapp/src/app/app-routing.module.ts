import { NgModule } from '@angular/core';
import { PreloadAllModules, Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './shared/components/login/login.component';
import { LandingComponent } from './shared/components/landing/landing.component';
import { ProfileComponent } from './shared/components/profile/profile.component';
import { PageNotFoundComponent } from './shared/components/pagenotfound/pagenotfound.component';

import {AuthGuard} from './shared/guards/auth.guard';
import { TableComponent } from './shared/components/table/table.component';


const routes: Routes = [
  { path: 'crisis-center', component: LoginComponent },
  { path: 'login', component: LoginComponent},
  { path: 'table', component: TableComponent},
  { path: 'profile', component: ProfileComponent,  canActivate: [
    AuthGuard
  ] },
  { path: 'hero/:id',      component: LoginComponent },
  {
    path: 'landing',
    component: LandingComponent,
    data: { title: 'Heroes List' }
  },
  { path: '',
    redirectTo: '/landing',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules } /* , { enableTracing: true }  */ )],
  exports: [RouterModule]
})
export class AppRoutingModule { }


