import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { MfylibModule } from 'mfylib';



import { FormsModule } from '@angular/forms';

import { MfyMaterialModule } from './shared/modules/mfymaterial/mfymaterial.module';
import { SharedtestModule } from './shared/modules/sharedtest/sharedtest.module';
import { LoginComponent } from './shared/components/login/login.component';
import { LogoutComponent } from './shared/components/logout/logout.component';
import { SignupComponent } from './shared/components/signup/signup.component';
import { LandingComponent } from './shared/components/landing/landing.component';
import { PageNotFoundComponent } from './shared/components/pagenotfound/pagenotfound.component';
import { TableComponent } from './shared/components/table/table.component';
import { DataTableComponent } from './shared/components/datatable/datatable.component';
import { LoginService } from './shared/services/login.service';
import { AppUserService } from './shared/services/appuser.service';
import { DummyService } from './shared/services/dummy.service';

import { HttpClient } from '@angular/common/http';
import { HttpHandler } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { ProfileComponent } from './shared/components/profile/profile.component';
import { AuthService } from './shared/services/auth.service';

import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { UsersWebService } from './shared/services/users-web.service';
import { UsersDatasourceService } from './shared/services/users-datasource.service';
import { ErrorInterceptor } from './shared/components/interceptors/error.interceptor';
import { NoopInterceptor } from './shared/components/interceptors/noop.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LogoutComponent,
    SignupComponent,
    LandingComponent,
    PageNotFoundComponent,
    ProfileComponent,
    TableComponent,
    DataTableComponent




  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    MfyMaterialModule,
    SharedtestModule,
    HttpClientModule


  ],
  providers:[ NoopInterceptor, HttpClient,  /*ErrorInterceptor, */ UsersDatasourceService, UsersWebService,FormBuilder, AuthService, LoginService, AppUserService, DummyService ],
  bootstrap: [AppComponent]
})
export class AppModule { }

