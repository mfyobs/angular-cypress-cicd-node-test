import { Injectable } from '@angular/core';
import { DataSource } from '@angular/cdk/table';
import { AppUser } from '../models/appuser.model';
import { BehaviorSubject, of, Observable } from 'rxjs';
import { CollectionViewer } from '@angular/cdk/collections';
import { catchError, tap, map, finalize } from 'rxjs/operators';
import { UsersWebService } from './users-web.service';

@Injectable({
  providedIn: 'root'
})
export class UsersDatasourceService  implements DataSource<AppUser> {

  private lessonsSubject = new BehaviorSubject<AppUser[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private usersWebService: UsersWebService) {}

  connect(collectionViewer: CollectionViewer): Observable<AppUser[]> {
      return this.lessonsSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
      this.lessonsSubject.complete();
      this.loadingSubject.complete();
  }

  loadLessons(courseId: number, filter = '',
              sortDirection = 'asc', pageIndex = 0, pageSize = 3) {

      this.loadingSubject.next(true);

      this.usersWebService.findUsers(courseId, filter, sortDirection,
          pageIndex, pageSize).pipe(
          catchError(() => of([])),
          finalize(() => this.loadingSubject.next(false))
      )
      .subscribe(lessons => this.lessonsSubject.next(lessons));
  }

}
