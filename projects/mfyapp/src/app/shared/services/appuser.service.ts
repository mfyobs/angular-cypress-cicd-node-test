import { Injectable } from '@angular/core';
import { AppUser } from '../models/appuser.model';

@Injectable()
export class AppUserService {

  private isUserLoggedIn;
  public usserLogged: AppUser;

  constructor() {
    this.isUserLoggedIn = false;
  }

  setUserLoggedIn(user: AppUser) {
    this.isUserLoggedIn = true;
    this.usserLogged = user;
    localStorage.setItem('currentUser', JSON.stringify(user));

  }

  getUserLoggedIn() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

}
