import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AppUser } from '../models/appuser.model';
import { Observable } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersWebService {

  constructor(private http: HttpClient) {}

  findUsers(
      courseId: number, filter = '', sortOrder = 'asc',
      pageNumber = 0, pageSize = 3):  Observable<AppUser[]> {

      return this.http.get('/api/lessons', {
          params: new HttpParams()
              .set('courseId', courseId.toString())
              .set('filter', filter)
              .set('sortOrder', sortOrder)
              .set('pageNumber', pageNumber.toString())
              .set('pageSize', pageSize.toString())
      }).pipe(
          map(res =>  res['payload'])
      );
  }

}
