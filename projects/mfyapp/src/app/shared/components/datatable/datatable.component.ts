import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UsersDatasourceService } from '../../services/users-datasource.service';

@Component({
  selector: 'app-datatable',
  templateUrl: './datatable.component.html',
  styleUrls: ['./datatable.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DataTableComponent implements OnInit {

  displayedColumns: string[] = ['position', 'userName', 'password'];
  dataSource = UsersDatasourceService;

  constructor() { }

  ngOnInit() {
  }

  onRowClicked(row) {
    console.log('Row clicked: ', row);
  }

}

