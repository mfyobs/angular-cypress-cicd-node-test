import { Component,ChangeDetectionStrategy, OnInit } from '@angular/core';

export interface tableElement {
  position: number;
  userName: string;
  password: string;
}

const TABLE_DATA: tableElement[] = [
  {position: 1, userName: "user1", password: "password1"},
  {position: 2, userName: "user2", password: "password2"}
]

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class TableComponent implements OnInit {

  displayedColumns: string[] = ['position', 'userName', 'password'];
  dataSource = TABLE_DATA;

  constructor() { }

  ngOnInit() {
  }

  onRowClicked(row) {
    console.log('Row clicked: ', row);
  }

}
