import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { AppUser } from '../../models/appuser.model';
import { LoginService } from '../../services/login.service';
import { AppUserService } from '../../services/appuser.service';
import { DummyService } from '../../services/dummy.service';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
//  providers: [ LoginService ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';


  public username: string;
  public password: string;

  constructor( private loginService: LoginService, private formBuilder: FormBuilder, private dummyService: DummyService,  private userService: AppUserService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });
  }


  get f() { return this.loginForm.controls; }


  login() {
    console.log('login click');
    console.log(this.username);
    console.log(this.password);

    this.loginService.echo();

   /* this.loginService.login(this.username, this.password).subscribe(

      res => {
     //   let u: AppUser = {userName: this.username};
     //  this.userService.setUserLoggedIn(u);

      },
      error => {
        console.error(error);

      },
      () => this.navigate()
    );*/







  }

  navigate() {
    console.log('navigate'); }


}
