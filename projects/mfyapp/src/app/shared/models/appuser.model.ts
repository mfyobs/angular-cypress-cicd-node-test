export class AppUser {
  id: number;
  userName: string;
  email: string;
  phone: string;
  password: string;
  firstName: string;
  lastName: string;
  JWTtoken?: string;
}
