import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MfylibComponent } from './mfylib.component';

describe('MfylibComponent', () => {
  let component: MfylibComponent;
  let fixture: ComponentFixture<MfylibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MfylibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MfylibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
